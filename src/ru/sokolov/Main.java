package ru.sokolov;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Продолжите цифровой ряд!");

        int k = 1;
        for (int i = 1; i < 5; i++) {
            k += 2;
            System.out.print(k);
            System.out.print(" ");
            k *= 2;
            if (i != 4) {
                System.out.print(k);
                System.out.print(" ");
            }
        }
        System.out.println();
        System.out.println("Введите ваше число:");
        Scanner scanner = new Scanner(System.in);
        while (scanner.nextInt() != k) {
            System.out.println("Вы не угадали, попробуйте еще раз!");
        }
        System.out.println("Правильный ответ!");

    }
}
